# -*- coding: utf-8 -*-
import os, json, codecs, datetime
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, jsonify
import hashlib
from werkzeug.utils import secure_filename
from app import app
import pandas as pd
import numpy as np
from neupy import algorithms, environment
from sklearn.preprocessing import MinMaxScaler

def connect_excel(excel=''):
    if excel == '' or os.path.isfile(excel) != True:
        excel = app.config['DATAEXCEL']
    data = pd.read_excel(excel)
    # datakita = data.to_records()
    return data

def bulan(bln):
    bulan_arr={1: "Januari",'01':"Januari", 2:"Februari",'02':"Februari",3:"Maret",'03':"Maret", 4:"April",'04':"April", 5:"Mei", '05':"Mei", 6:"Juni", '06':"Juni", 7:"Juli", '07':"Juli", 8:"Agustus", '08':"Agustus", 9:"September",'09':"September", 10:"Oktober",'10':"Oktober", 11:"November", '11':"November",12:"Desember",'12':"Desember"}
    return bulan_arr[bln]

def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

def safe_div(x,y):
    if y == 0:
        return 0
    else:
        return x/y

def auth_check(level):
    granted = 0
    if session['logged_in']:
        for lv in level:
            if lv == session['level']:
                granted = granted + 1
    if granted > 0:
        return True
    else:
        return False


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.route('/')
def index():
    return render_template("index.html")

@app.route('/about')
def about():
    db = get_db()
    cur = db.execute('select * from user')
    users = cur.fetchall()
    return render_template("about.html", users=users)

@app.route('/kelolauser')
def kelolauser():
    if auth_check([1]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    db = get_db()
    cur = db.execute('select * from user')
    users = cur.fetchall()
    return render_template("kelolauser.html", users=users)

@app.route('/tambahuser', methods=['GET','POST'])
def tambahuser():
    if auth_check([1]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    if request.method == 'POST':
        db = get_db()
        username = request.form['username']
        passwd = hashlib.md5(request.form['password']).hexdigest()
        jabat = request.form['level']
        db.execute('insert into user (username, password,jabatan) values (?,?,?)', [username, passwd, jabat])
        db.commit()
        flash("Pengguna Baru Berhasil Dibuat!")
        return redirect(url_for('kelolauser'))
    else:
        preform = {'id':'','username':'','password':'','jabatan':'1'}
        return render_template("form_user.html", isi=preform, judul="Tambah Pengguna", valsubmit="Tambah", actsubmit="tambahuser")

@app.route('/edituser', methods=['GET','POST'])
def edituser():
    if auth_check([1]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    if request.method == 'POST':
        db = get_db()
        iduser = request.form['id']
        username = str(request.form['username'])
        jabat = request.form['level']
        db.execute("update user set username = ?, jabatan = ? where id = ?", [username, jabat, iduser])
        db.commit()
        flash("Data Pengguna Berhasil Dibuat!")
        return redirect(url_for('kelolauser'))
    else:
        db = get_db()
        cur = db.execute('select * from user where id = ?',request.args['id'])
        user = cur.fetchall()
        return render_template('form_user.html', isi=user[0], judul="Ubah Data Pengguna", valsubmit="Simpan", actsubmit="edituser")


@app.route('/hapususer')
def hapususer():
    if auth_check([1]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    if request.args['id'] != '':
        db = get_db()
        db.execute('delete from user where id = ?',request.args['id'])
        db.commit()
        flash("Pengguna telah dihapus!")
        return redirect(url_for('kelolauser'))

@app.route('/prakiraanhari')
def prakiraanhari():
    if auth_check([2]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    return render_template("prakiraan_hari.html")

@app.route('/prakiraanbulan', methods=['GET','POST'])
def prakiraanbulan():
    if auth_check([2]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    else:
        db = get_db()
        namafile = os.path.join(app.config['UPLOAD_FOLDER'],'data-uji.xlsx')
        if request.method == 'POST':
            if 'berkas' not in request.files:
                flash('Tidak ada file yang dikirim')
                return redirect(url_for('prakiraanbulan'))
            file = request.files['berkas']
            if file.filename == '':
                flash('Tidak ada file terpilih')
                return redirect(url_for('prakiraanbulan'))
            if file and allowed_file(file.filename):
                ## filename = secure_filename(file.filename)
                errsql = ''
                file.save(namafile)
        
            datatest = pd.read_excel(namafile)
            haduhtest = pd.DataFrame(data={'angin':datatest.Angin.astype(float), 'temperatur':datatest.Temperatur.astype(float), 'tekanan':datatest.Tekanan.astype(float), 'kelembapan':datatest.Kelembapan.astype(float),'tanggal':datatest.Tanggal.astype(str)}, columns=['angin', 'temperatur','tekanan','kelembapan','tanggal'])
            db.execute('delete from cuaca_testing')
            db.executemany("insert into cuaca_testing (angin,temperatur,tekanan_udara,kelembapan,tanggal) values (?,?,?,?,?)", haduhtest.to_records(index=False))
            
            databulan = pd.read_sql("select angin,temperatur,tekanan_udara,kelembapan,cuaca from cuaca_training where strftime('%m',tanggal) between strftime('%m','"+min(haduhtest['tanggal'])+"') and strftime('%m','"+max(haduhtest['tanggal'])+"')", db)
            testinglagi = pd.read_sql("select angin,temperatur,tekanan_udara,kelembapan,id from cuaca_testing", db)
            raw_appended = databulan.iloc[:,0:4].copy().append(testinglagi.iloc[:,0:4].copy())
            mixmax = MinMaxScaler()
            datakita = mixmax.fit_transform(raw_appended)
            stdev = float(request.form['spread'])
            #environment.reproducible()
            pnn = algorithms.PNN(std=stdev)
            pnn.train(datakita[:databulan.shape[0]], databulan['cuaca'].astype(int))
            y_predicted = pnn.predict(datakita[databulan.shape[0]:])
            db.executemany('update cuaca_testing set cuaca = ? where id = ?', pd.DataFrame(data={'cuaca':y_predicted,'id':testinglagi['id'].astype(int)}, columns=['cuaca','id']).to_records(index=False))
            db.commit()
        cuaca = ['','Cerah','Hujan Ringan','Hujan Sedang','Hujan Lebat','Hujan Sangat Lebat']
        datatest = pd.read_sql('select * from cuaca_testing',db)
        return render_template('prakiraan_bulan.html',dataraw=datatest.to_records(), pcuaca=cuaca, file_ada=os.path.isfile(namafile))

@app.route('/grafbulan', methods=['GET','POST'])
def grafbulan():
    if auth_check([2]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    else:
        db = get_db()
        sekarang = datetime.date.today()
        if request.method == 'POST':
            sekarang = datetime.date(2017,int(request.form['bulan']),1)
        data = pd.read_sql('select strftime("%Y",tanggal) as tahun, avg(angin) as av_angin, avg(kelembapan) as av_lembap, avg(temperatur) as av_temp, avg(tekanan_udara) as av_udara from cuaca_training where strftime("%m",tanggal) = "'+sekarang.strftime("%m")+'" group by strftime("%Y",tanggal)',db)
        datac = pd.read_sql('select cuaca, count(*) as jm_cuaca from cuaca_training where strftime("%m",tanggal) = "'+sekarang.strftime("%m")+'" group by cuaca',db)
        jumlah = [datac[datac['cuaca'] == x]['jm_cuaca'][x-1] for x in range(1,datac.shape[0]+1)]
        mixmax = MinMaxScaler(feature_range=(0,100))
        datanorm = pd.DataFrame(data=mixmax.fit_transform(data.iloc[:,1:5]), columns=['angin','kelembapan','temperatur','tekanan_udara'])
        hasil = {
            'jumlah_cuaca':jumlah,
            'label_hari':data.tahun,
            'angin_norm':datanorm.angin.to_json(orient='values'),
            'temperatur_norm':datanorm.temperatur.to_json(orient='values'),
            'tekanan_udara_norm':datanorm.tekanan_udara.to_json(orient='values'),
            'kelembapan_norm':datanorm.kelembapan.to_json(orient='values'),
            'angin':data.av_angin.to_json(orient='values'),
            'temperatur':data.av_temp.to_json(orient='values'),
            'tekanan_udara':data.av_udara.to_json(orient='values'),
            'kelembapan':data.av_lembap.to_json(orient='values'),
            'min_year':min(data.tahun),
            'max_year':max(data.tahun),
            'month':bulan(sekarang.month),
            'raw_month':sekarang.month
        }
        return render_template('graf_bulan.html', dataparam=hasil)

@app.route('/graftahun', methods=['GET','POST'])
def graftahun():
    if auth_check([2]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    db = get_db()
    tahun_list = pd.read_sql('select strftime("%Y", tanggal) as tahun from cuaca_training group by strftime("%Y", tanggal)',db)
    #sekarang = datetime.date(int(max(tahun_list.tahun)),1,1)
    sekarang = datetime.date(2016,1,1)
    if request.method == 'POST':
        sekarang = datetime.date(int(request.form['tahun']),1,1)
    data = pd.read_sql('select strftime("%m",tanggal) as bulan, avg(angin) as av_angin, avg(kelembapan) as av_lembap, avg(temperatur) as av_temp, avg(tekanan_udara) as av_udara from cuaca_training where strftime("%Y",tanggal) = "'+str(sekarang.year)+'" group by strftime("%m",tanggal)',db)
    datac = pd.read_sql('select cuaca, count(*) as jm_cuaca from cuaca_training where strftime("%Y",tanggal) = "'+str(sekarang.year)+'" group by cuaca',db)
    jumlah = {}
    count=0
    for ix in range(0,5):
        jumlah[ix+1] = 0 if datac[datac['cuaca'] == ix+1].empty else datac[datac['cuaca'] == ix+1]['jm_cuaca'][count]
        if not datac[datac['cuaca'] == ix+1].empty:
            count+=1

    #jumlah = [datac[datac['cuaca'] == x]['jm_cuaca'][x-1] for x in range(1,datac.shape[0]+1)]
    mixmax = MinMaxScaler(feature_range=(0,100))
    datanorm = pd.DataFrame(data=mixmax.fit_transform(data.iloc[:,1:5]), columns=['angin','kelembapan','temperatur','tekanan_udara'])
    label_bulan = [bulan(str(x)) for x in data.bulan]
    hasil = {
        'jumlah_cuaca':jumlah,
        'label_hari':label_bulan,
        'angin_norm':datanorm.angin.to_json(orient='values'),
        'temperatur_norm':datanorm.temperatur.to_json(orient='values'),
        'tekanan_udara_norm':datanorm.tekanan_udara.to_json(orient='values'),
        'kelembapan_norm':datanorm.kelembapan.to_json(orient='values'),
        'angin':data.av_angin.to_json(orient='values'),
        'temperatur':data.av_temp.to_json(orient='values'),
        'tekanan_udara':data.av_udara.to_json(orient='values'),
        'kelembapan':data.av_lembap.to_json(orient='values'),
        'min_month':bulan(min(data.bulan)),
        'max_month':bulan(max(data.bulan)),
        'min_thn':int(min(tahun_list.tahun)),
        'max_thn':int(max(tahun_list.tahun)),
        'year':sekarang.year
    }
    return render_template('graf_tahun.html', dataparam=hasil)

@app.route('/aksikirain', methods=['POST'])
def aksikirain():
    data_is_num = True;
    try:
        float(request.form['temperatur'])
        float(request.form['tekanan'])
        float(request.form['lembap'])
        float(request.form['angin'])
        float(request.form['spread'])
    except ValueError:
        data_is_num = False;
    if data_is_num:
        db = get_db()
        namafile = os.path.join(app.config['UPLOAD_FOLDER'],'data-latih.xlsx')
        # cuaca_tnorm = pd.read_sql('select temperatur,tekanan_udara,kelembapan,angin from cuaca_training',db)
        # cuaca_tkls = pd.read_sql('select cuaca from cuaca_training',db)
        cuaca_testing = {'Temperatur':request.form['temperatur'],'Tekanan':request.form['tekanan'],'Kelembapan':request.form['lembap'],'Angin':request.form['angin']}
        cuaca_raw = pd.read_excel(namafile)
        X_raw = pd.DataFrame(data={'Temperatur':cuaca_raw.Temperatur, 'Tekanan':cuaca_raw.Tekanan,'Kelembapan':cuaca_raw.Kelembapan,'Angin':cuaca_raw.Angin}, columns=['Temperatur','Tekanan','Kelembapan','Angin'])
        raw_appended = X_raw.append(cuaca_testing, ignore_index=True)
        mixmax = MinMaxScaler()
        datakita = mixmax.fit_transform(raw_appended)
        stdev = float(request.form['spread'])
        environment.reproducible()
        pnn = algorithms.PNN(std=float(stdev))
        pnn.train(datakita[:datakita.shape[0]-1], cuaca_raw.Cuaca.astype(int))
        y_predicted = pnn.predict(pd.DataFrame(data=[datakita[datakita.shape[0]-1]]))
        # metrics.accuracy_score(y_test, y_predicted)
        return render_template('tester.html', hasil=y_predicted[0])
    else:
        return render_template('tester.html', hasil="0")

@app.route('/coba', methods=['POST'])
def coba():
    hasil = request.form['temperatur']
    return render_template('tester.html', hasil=hasil)

@app.route('/datalatih')
def datalatih():
    if auth_check([1]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    namafile = os.path.join(app.config['UPLOAD_FOLDER'],'data-latih.xlsx')
    #data = connect_excel(namafile)
    #mixmax = MinMaxScaler()
    #datakita = mixmax.fit_transform(pd.DataFrame(data={'temperatur':data['Temperatur'],'tekanan':data['Tekanan'],'kelembapan':data['Kelembapan'],'angin':data['Angin']}, columns=['temperatur','tekanan','kelembapan','angin']))
    db = get_db()
    data = pd.read_sql('select * from cuaca_training',db)
    jenis = 'latih'
    cuaca = ['','Cerah','Hujan Ringan','Hujan Sedang','Hujan Lebat','Hujan Sangat Lebat']
    return render_template("pilihcsv.html", dataraw=data.to_records(), jenisdata=jenis, file_ada=os.path.isfile(namafile), pcuaca=cuaca)

@app.route('/editdata', methods=['GET','POST'])
def editdata():
    if auth_check([1]) != True:
        flash("Anda tidak memiliki akses untuk mengakses menu ini")
        return redirect(url_for('index'))
    db = get_db()
    if request.method == 'POST':
        errsql = ''
        try:
            db.execute("update cuaca_training set tanggal = ?, temperatur = ?, tekanan_udara = ?, kelembapan = ?, angin = ?, cuaca = ? where id = ?",[request.form['tanggal'],request.form['temperatur'],request.form['tekanan'],request.form['lembap'],request.form['angin'],request.form['cuaca'], request.form['id']])
        except sqlite3.Error as e:
            errsql = e.args[0]
        if errsql == '':
            flash("Berhasil menyimpan data")
        else:
            flash("Terdapat kesalahan selama menyimpan data")
        db.commit()
        return redirect(url_for('datalatih'))
    else:
        data = pd.read_sql('select * from cuaca_training where id = "'+request.args['id']+'"',db)
        return render_template('edit_data.html', data=data)

@app.route('/hapusdata')
def hapusdata():
    db = get_db()
    errsql = ''
    try:
        db.execute("delete from cuaca_training where id = ?",[request.args['id']])
    except sqlite3.Error as e:
        errsql = e.args[0]
    if errsql == '':
        flash("Berhasil menghapus data")
    else:
        flash("Terdapat kesalahan selama menyimpan data")
    db.commit()
    return redirect(url_for('datalatih'))

@app.route('/unggahdata', methods=['POST'])
def unggahdata():
    if request.form['jenisdata'] == 'latih':
        filename = 'data-latih.xlsx'
        hal = 'datalatih'
    else:
        filename = 'data-uji.xlsx'
        hal = 'datauji'
    if 'berkas' not in request.files:
        flash('Tidak ada file yang dikirim')
        return redirect(url_for(hal))
    file = request.files['berkas']
    if file.filename == '':
        flash('Tidak ada file terpilih')
        return redirect(url_for(hal))
    if file and allowed_file(file.filename):
        ## filename = secure_filename(file.filename)
        errsql = ''
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        data = pd.read_excel(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        datalagi = pd.DataFrame(data={'angin':data.Angin.astype(float), 'cuaca':data.Cuaca.astype(int),
        'temperatur':data.Temperatur.astype(float), 'tekanan':data.Tekanan.astype(float), 
        'kelembapan':data.Kelembapan.astype(float),'tanggal':data.Tanggal.astype(str)}, 
        columns=['angin', 'cuaca', 'temperatur','tekanan','kelembapan','tanggal'])
        konek = sqlite3.connect(app.config['DATABASE'])
        try:
            konek.executemany("insert into cuaca_training (angin,cuaca,temperatur,tekanan_udara,kelembapan,tanggal) values (?,?,?,?,?,?)", datalagi.to_records(index=False))
        except sqlite3.Error as e:
            errsql = e.args[0]
        if errsql != '':
            flash('Data berhasil diunggah')
        else:
            flash(errsql)
        konek.commit()
        konek.close()
    else:
        flash('File yang dikirim tidak diperbolehkan')
    return redirect(url_for(hal))

@app.route('/pelatihan', methods=['GET','POST'])
def pelatihan():
    if auth_check([1]) != True:
        return redirect(url_for('login'))
    namafile = os.path.join(app.config['UPLOAD_FOLDER'],'data-latih.csv')
    data = connect_excel(namafile)
    mixmax = MinMaxScaler()
    datakita = mixmax.fit_transform(pd.DataFrame(data={'production':data['PROUCTION'],'panen':data['PANEN']}, columns=['production','panen']))
    file_json = os.path.join(app.root_path,'log.json')
    json_raw = codecs.open(file_json,'r',encoding='utf-8').read()
    param = json.loads(json_raw)
    return render_template('pelatihan.html', dataraw=data, datanorm=datakita, dataparam=param, hitungan=range(len(datakita)))

@app.route('/latihaja',  methods=['POST'])
def latihaja():
    i_node = int(request.form['in_node_param'])
    h_node = int(request.form['hnode_param'])
    max_ep = int(request.form['maxepoch_param'])
    lrate  = float(request.form['lrate_param'])
    namafile = os.path.join(app.config['UPLOAD_FOLDER'],'data-latih.csv')
    data = connect_excel(namafile)
    hasilnya = rbfnn.train(data['PROUCTION'], data['PANEN'], tanggal=data['DATE'], n_hidden=h_node, input_node=i_node, ephocs=max_ep, lr=lrate)
    json.dump(hasilnya, codecs.open('app/log.json', 'w', encoding='utf-8'), separators=(',',':'), sort_keys=True, indent=4)
    #return jsonify(hasilnya)
    return redirect(url_for('pelatihan'));

@app.route('/ujiaja', methods=['GET','POST'])
def ujiaja():
    i_node = int(request.form['in_node_param'])
    h_node = int(request.form['hnode_param'])
    max_ep = int(request.form['maxepoch_param'])
    lrate  = float(request.form['lrate_param'])
    namafile = os.path.join(app.config['UPLOAD_FOLDER'],'data-uji.csv')
    data = connect_excel(namafile)
    file_json = os.path.join(app.root_path,'log.json')
    json_raw = codecs.open(file_json,'r',encoding='utf-8').read()
    param = json.loads(json_raw)
    hasilnya = rbfnn.test(data['PROUCTION'], data['PANEN'], param['weight'], param['center'], tanggal=data['DATE'], n_hidden=h_node, input_node=i_node, ephocs=max_ep, lr=lrate)
    json.dump(hasilnya, codecs.open('app/log-uji.json', 'w', encoding='utf-8'), separators=(',',':'), sort_keys=True, indent=4)
    #return jsonify(hasilnya)
    return redirect(url_for('pengujian'));


@app.route('/pengujian', methods=['GET','POST'])
def pengujian():
    if auth_check([1]) != True:
        return redirect(url_for('login'))
    namafile = os.path.join(app.config['UPLOAD_FOLDER'],'data-uji.csv')
    data = connect_excel(namafile)
    mixmax = MinMaxScaler()
    datakita = mixmax.fit_transform(pd.DataFrame(data={'production':data['PROUCTION'],'panen':data['PANEN']}, columns=['production','panen']))
    file_json = os.path.join(app.root_path,'log-uji.json')
    json_raw = codecs.open(file_json,'r',encoding='utf-8').read()
    param = json.loads(json_raw)
    file_json = os.path.join(app.root_path,'log.json')
    json_raw = codecs.open(file_json,'r',encoding='utf-8').read()
    paramlatih = json.loads(json_raw)
    return render_template('pengujian.html', dataraw=data, datanorm=datakita, dataparam=param, paramlatih=paramlatih, hitungan=range(len(datakita)))


@app.route('/login', methods=['GET','POST'])
def login():
    if session == None:
        return redirect(url_for('index'))
    error = None
    if request.method == 'POST':
        db = get_db()
        cur = db.execute('select * from user where username = ?', [request.form['username']])
        fetched = cur.fetchall()
        if len(fetched) != 0:
            user = fetched[0]
            if hashlib.md5(request.form['password']).hexdigest() == user['password']:
                session['logged_in'] = True
                session['level'] = user['jabatan']
                session['usrlog'] = user['username']
                flash("Anda telah berhasil masuk")
                return redirect(url_for('index'))
            else:
                flash("Username/Password yang Anda masukkan salah")
        else:
            flash("Username/Password yang Anda masukkan salah")
    return render_template('login.html', error=error)

@app.route('/tentangsistem')
def tentangsistem():
    return render_template('tentangsistem.html')

@app.route('/logout')
def logout():
    session.pop('logged_in',None)
    session.pop('level',None)
    session.pop('usrlog',None)
    flash("Anda telah berhasil keluar")
    return redirect(url_for('index'))